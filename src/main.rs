use axum::{
	extract::{Json, Query, State},
	routing::get,
	Router,
};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::{
	collections::HashMap, fs::OpenOptions, path::Path, sync::{Arc, Mutex}, thread::sleep, time::Duration
};

#[derive(Default, Deserialize, Serialize)]
struct Polls(Mutex<HashMap<String, HashMap<String, usize>>>);

fn save_state(poll: State<Arc<Polls>>) {
	let file = OpenOptions::new().truncate(true).write(true).open("poll").unwrap();
	let obj = Arc::<Polls>::into_inner(poll.0).unwrap();
	let value = serde_json::to_value(obj).unwrap();
	serde_json::to_writer(&file, &value).unwrap();
}

fn create_poll(data: &mut State<Arc<Polls>>, query_poll: String, owner: String) -> String {
	let polls = data.0.0.lock().unwrap();
	for mut poll in polls.iter() {
		if poll.0 == &query_poll {
			return String::new()
		}
		poll.0 = &owner;
		poll.1 = &HashMap::new();
	}
	String::new()
}

fn add_entry(data: State<Arc<Polls>>, query_poll: String, entry: String) -> String {
	let mut polls = data.0.0.lock().unwrap();
	for poll in polls.iter_mut() {
		if poll.0 != &query_poll { continue }
		if !poll.1.contains_key(&entry) {
			poll.1.insert(entry.clone(), 0); 
			return format!("added {}", poll.1.get_key_value(&entry).unwrap().0)
		}
	}
	String::new()
}

async fn list_polls(poll: State<Arc<Polls>>) -> Json<Value> {
	let mut hm = HashMap::new();
	let polls = poll.0.0.lock().unwrap();
	for poll in polls.iter() {
		hm.insert(poll.0, poll.1);
	}
	Json(serde_json::to_value(hm).unwrap())
}

fn add_vote(
	data: &mut State<Arc<Polls>>,
	query_poll: String,
	user: String,
) -> String {
	let mut polls = data.0.0.lock().unwrap();
	for poll in polls.iter_mut() {
		if poll.0 != &query_poll { continue } 
		for (k, v) in poll.1.clone().iter_mut() {
			if k == &user {
				poll.1.insert(k.to_string(), 1 + *v);
				return "voted".to_string();
			}
		}
	}
	String::new()
}

async fn poll_handler(mut poll: State<Arc<Polls>>, action: Query<(String, String)>, query: Query<(String, String)>) -> Json<String> {
	let action = action.0;
	let query = query.0;
	let res = match (action.0.as_str(), action.1.as_str()) {
		("create", "poll") => create_poll(&mut poll, query.0.clone(), query.1.clone()),
		("vote", "poll") => add_vote(&mut poll, query.0.clone(), query.1.clone()),
		("list", "polls") => list_polls(poll).await.to_string(),
		_ => add_entry(poll, query.0.clone(), query.1.clone()),
	};
	Json(res)
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> std::io::Result<()> {
	let app_data = if Path::is_file(Path::new("poll")) {
		let file = OpenOptions::new().read(true).open("poll").unwrap();
		Arc::new(serde_json::from_reader(file).unwrap())
	} else {
		Arc::new(Polls::default())
	};
	let app = Router::new()
		.route("/", get(list_polls))
		.route("/poll", get(poll_handler))
		.with_state(Arc::clone(&app_data));

	tokio::spawn( async {
		save_state(State(app_data));
		sleep(Duration::from_secs(30));
	});

	let listener = tokio::net::TcpListener::bind("0.0.0.0:14099").await?;
	axum::serve(listener, app).await?;
	Ok(())
}
